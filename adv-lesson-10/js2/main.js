import { loadProducts } from "./api.js"
import { createCard } from "./functions.js"


loadProducts().then(data => {
    data.forEach(item => {
        createCard(
            item.title,
            item.price,
            item.thumbnail
        )
    });
})



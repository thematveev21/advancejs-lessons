import config from './config.js'


const root = document.getElementById(config.ROOT_ELEMENT_ID)

function addCard(cardElement){
    root.append(cardElement)
}


function createCard(title, price, image){
    let card = document.createElement('div')
    card.classList.add('card')

    let img = document.createElement('img')
    img.src = image

    let name = document.createElement('h2')
    name.textContent = title

    let pr = document.createElement('p')
    pr.textContent = price

    card.append(img)
    card.append(name)
    card.append(pr)
    addCard(card)
    return card

}

export {createCard}


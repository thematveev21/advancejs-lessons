import config from './config.js'


export async function loadProducts(){
    let response = await fetch(config.API_URL)
    let data = await response.json()
    return data.products
}


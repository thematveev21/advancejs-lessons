import {API_URL} from './config.js'

function convertToJson(text){
    return JSON.parse(text)
}

function getProducts(){
    return new Promise(function(resolve, reject){
        let request = new XMLHttpRequest();
        request.open('GET', API_URL)
        request.onload = () => resolve(convertToJson(request.responseText))
        request.onerror = () => reject()
        request.send()
    })
}


export {
    getProducts
}



// function Student(username, ...points){
//     this.username = username;
//     this.points = [12, ...points];
// }

// Student.prototype.getAvgPoinst = function(){
//     let sum = 0;

//     for(let p of this.points){
//         sum += p
//     }

//     return sum / this.points.length
// }


// let s1 = new Student("Maksim", 12, 11, 12);

// let {getAvgPoinst} =  s1;

// // getAvgPoinst.call(s1)

// getAvgPoinst = getAvgPoinst.bind(s1);




class Student {
    constructor(username){
        this.username = username
        this._points = [];
    }

    addPoint(lessonName, point){
        this._points.push({
            lessonName, point
        })
    }


}


let s1 = new Student("TestStudent1")
s1.addPoint("ComputerNetworks", 5)
s1.addPoint("History", 3)
s1.addPoint("History", 4)
s1.addPoint("History", 4)
s1.addPoint("ComputerNetworks", 3)
s1.addPoint("History", 2)
class Student {
    constructor(username){
        this.username = username
        this._points = [];
    }

    addPoint(lessonName, point){
        this._points.push({
            lessonName, point
        })
    }

    get points(){
        return this._points
    }


}




function renderStudent(student){
    let container = document.createElement('div')
    container.classList.add('student')

    let nameTitle = document.createElement('h2')

    nameTitle.classList.add('student-name')
    nameTitle.textContent = student.username
    let table = document.createElement('table')

    container.append(nameTitle)
    container.append(table)

    let tableHeader = document.createElement('tr')
    let lessonHeader = document.createElement('th')
    let pointHeader = document.createElement('th')
    lessonHeader.textContent = "Lesson Name"
    pointHeader.textContent = 'Point'
    tableHeader.append(lessonHeader)
    tableHeader.append(pointHeader)

    table.append(tableHeader);

    for(let lesson of student.points){
        console.log(lesson)
        let r = document.createElement('tr')

        let name = document.createElement('td')
        name.textContent = lesson.lessonName

        let point = document.createElement('td')
        point.textContent = lesson.point

        r.append(name)
        r.append(point)

        table.append(r);
    }


    return container

}


let s1 = new Student("TestStudent1")
s1.addPoint("ComputerNetworks", 5)
s1.addPoint("History", 3)
s1.addPoint("History", 4)
s1.addPoint("History", 4)
s1.addPoint("ComputerNetworks", 3)
s1.addPoint("History", 2)


let s2 = new Student("TestStudent2")
s2.addPoint("ComputerNetworks", 5)
s2.addPoint("History", 3)
s2.addPoint("Math", 4)


document.body.append(renderStudent(s1))
document.body.append(renderStudent(s2))
// spread

// function calc(a, b, c, d){
//     return a + b - c / d;
// }

// let data = [12, 23, 34];


// console.log(
//     calc(...data)
// )


// example 2


// let data = [12, 23, 34];

// let data2 = [...data, 1, 2, 3, ...data, ...data]

// console.log(data2)


// example 3


// let text = "Hello world";

// let letters = [...text];

// console.log(letters);




// rest


// function calc(...nums){
    
//     let sum = 0;

//     for (let n of nums){
//         sum += n
//     }

//     console.log(sum);
// }


function printer(startLine, endLine, ...nums){
    console.log(startLine)
    for(let n of nums){
        console.log(n)
    }
    console.log(endLine)
}
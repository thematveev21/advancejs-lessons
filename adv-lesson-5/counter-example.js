class Counter {

    static counters = [];

    constructor(initialState=0){
        this._state = initialState;

        // create new counter in html

        this._counter = document.createElement('h2');
        this._counter.textContent = this._state;
        document.body.append(this._counter)

        Counter.counters.push(this)
    }


    _render(){
        this._counter.textContent = this._state;
    }


    plus(){
        this._state++
        this._render()
    }

    minus(){
        this._state--
        this._render()
    }


    get value(){
        return this._state;
    }

    set value(value){
        this._state = value;
        this._render()
    }

    static resetAllCounters(){
        for(let c of Counter.counters){
            c.value = 0;
        }
    }


}


let c1 = new Counter()
let c2 = new Counter()
const inputText = document.querySelector(".input-text")
const outputText = document.querySelector(".output-text")
const fromLang = document.querySelector(".from-lang")
const toLang = document.querySelector(".to-lang")
const translateBtn = document.querySelector(".translate-btn")


function addLangOption(lang){
    fromLang.append(new Option(lang, lang))
    toLang.append(new Option(lang, lang))
}


function loadSupportedLangs(){
    return fetch(
        "https://google-translate1.p.rapidapi.com/language/translate/v2/languages",
        {
            method: 'GET',
            headers: {
                "X-RapidAPI-Key": "d7a0edba2fmshb5939cf9d313cacp10e2d9jsn2964ededae8a",
                "X-RapidAPI-Host": "google-translate1.p.rapidapi.com"
            }
        }
    )
}


function translateText(text, from, to){

    let params = {
        q: text,
        target: to,
        source: from
    }

    console.log(params)

    return fetch(
        "https://google-translate1.p.rapidapi.com/language/translate/v2",
        {
            method: "POST",
            body: JSON.stringify(params),
            headers: {
                "X-RapidAPI-Key": "d7a0edba2fmshb5939cf9d313cacp10e2d9jsn2964ededae8a",
                "X-RapidAPI-Host": "google-translate1.p.rapidapi.com"
            }
        }
    )
}


loadSupportedLangs()
    .then(response => {
        console.log(response)
        return response.json()
    })
    .then(data => {
        data.data.languages.forEach(lang => {
            addLangOption(lang.language)
        })
    })


translateBtn.addEventListener('click', event => {
    let text = inputText.value
    let from = fromLang.value
    let to = toLang.value

    translateText(text, from, to).then(response => response.json()).then(data => console.log(data))
})
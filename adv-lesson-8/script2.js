const list = document.querySelector('ul')

function getAllUsers(){
    let url = "https://dummyjson.com/users"
    return fetch(url)
}

function addNewUser(firstName, lastName) {
    let payload = {firstName, lastName}

    return fetch(
        "https://dummyjson.com/users/add",
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(payload)
        }
    )
}


function askUser(){
    let n1 = prompt("FN")
    let n2 = prompt("LN")
    addNewUser(n1, n2).then(response => console.log(response))
}

getAllUsers()
    .then(response => response.json())
    .then(data => {
        data.users.forEach(user => {
            let li = document.createElement('li')
            li.textContent = user.firstName + " " + user.lastName
            list.append(li)
        });
    })



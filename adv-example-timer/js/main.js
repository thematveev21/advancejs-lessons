import Timer from './timer.js'
import { renderListItem } from './render.js'

const startBtn = document.querySelector('.start')
const timerValue = document.querySelector('.timer-time')
const stopBtn = document.querySelector('.stop')
const fixBtn = document.querySelector('.fix')
const resetBtn = document.querySelector('.reset')
const fixedList = document.querySelector('.fixed')

let interval
let timer = new Timer()

console.log(timer)

startBtn.addEventListener('click', () => {
    timer.startTimer.call(timer)

    interval = setInterval(
        function(){timerValue.textContent = timer.value},
        50
    )
})


stopBtn.addEventListener('click', () => {
    clearInterval(interval)
})

fixBtn.addEventListener('click', () => {
    fixedList.prepend(renderListItem(timer.value))
})

resetBtn.addEventListener('click', () => {
    clearInterval(interval)
    timer.resetTimer.call(timer)
    timerValue.textContent = timer.value
})
class Timer {
    constructor(){
        this._value = 0
        this.startTime = undefined
    }

    startTimer(){
        this.startTime = new Date()
    }

    _formatTime(value){
        const totalMinutes = Math.floor(value / 60);
        const seconds = value % 60;
        const hours = Math.floor(totalMinutes / 60);
        const minutes = totalMinutes % 60;
        return `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${String(seconds).padEnd(2, '0')}`
    }

    get value(){
        let current = new Date()
        this._value = (current - this.startTime)
        return this._formatTime(this._value / 1000)
    }

    resetTimer(){
        this._value = 0
    }
    

    
}

export default Timer
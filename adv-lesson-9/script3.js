// https://dummyjson.com/users
// https://dummyjson.com/posts

async function getUsers(){
    console.log("Start loading users")
    let response = await fetch("https://dummyjson.com/users")
    let data = await response.json()
    console.log("Users loaded!")
    return data.users
}

async function getPosts(){
    console.log("Start loading posts")
    let response = await fetch("https://dummyjson.com/posts")
    let data = await response.json()
    console.log("Posts loaded!")
    return data.posts
}


async function loadAllData(){
    let result = await Promise.all([getUsers(),getPosts()])
    console.log("Hello")
    return result
}



loadAllData().then(([users, posts]) => {
    console.log(users)
    console.log(posts)

    for (let {id, firstName, lastName} of users){
        console.log(firstName, lastName)
        let post = posts.find(p => p.userId === id)
        if (post){
            console.log(post.title)
        }
        else {
            console.log("No posts!")
        }
    }
})
let promise = new Promise(function(resolve, reject){
    console.log("Start connection...")
    setTimeout(()=> {console.log("Connected!"); resolve()}, 4500)
})


promise
.then(function(){
    console.log("Loading data start ....")

    return new Promise(function(resolve, reject){
        setTimeout(() => {console.log("Data loaded!"), resolve({name: "Maksim"})}, 3200)
    })
})

.then(function(data){
    console.log("Data parsing", data)
})

